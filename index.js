const moveUp = () => {
    var elem = document.getElementById('cell-x');
    var position = parseInt(window.getComputedStyle(elem).bottom);
    if(position < 300){
        var sum = position + 100;
        elem.style.bottom = sum + 'px';
    }
}

const moveDown = () => {
    var elem = document.getElementById('cell-x');
    var position = parseInt(window.getComputedStyle(elem).bottom);
    if(position > 0){
        var sum = position - 100;
        elem.style.bottom = sum + 'px';
    }
}

const moveRight = () => {
    var elem = document.getElementById('cell-x');
    var position = parseInt(window.getComputedStyle(elem).left);
    if(position < 300){
        var sum = position + 100;
        elem.style.left = sum + 'px';
    }
}

const moveLeft = () => {
    var elem = document.getElementById('cell-x');
    var position = parseInt(window.getComputedStyle(elem).left);
    if(position > 0){
        var sum = position - 100;
        elem.style.left = sum + 'px';
    }
}

